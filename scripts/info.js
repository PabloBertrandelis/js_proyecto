//SPORTS LIST CONTENT / secction: LIST
var infoList = [
    {name: "Jarama",
    date: {day: 2,month: 5,year: 2021},
    icon: "./assets/icons/helmet.svg",
    image: "https://foracircuit.files.wordpress.com/2013/10/jarama_2.jpg",
    text: `Arranca motores para toda una experiencia de <span class="special_font">motociclismo</span> en el circuito del Jarama. Cuatro horas de motociclismo en primera persona donde te pondrás en la piel de los mejores pilotos de MotoGP. Para poder participar en el evento es necesario llevar tu propio vehículo, y disponer del carnet de conducir necesario para pilotarlo (mínimo 45cv), y equipación de mono completo y casco de doble evilla por motivos de seguridad. Para más información, completa el formulario con el evento "jarama". Autovía A1, Km 28, 28700 San Sebastián de los Reyes, Madrid. España`},

    {name: "RedBull Holy Bike",
    date: {day: 3,month: 6,year: 2021},
    icon: "./assets/icons/mountain-bike.svg",
    image: "https://labicikleta.com/wp-content/uploads/2017/07/ModalidadesMTB.jpg",
    text: `La Pinilla hace de inmejorable anfitrión para los cientos de inscritos del <span class="special_font">Red Bull Holy Bike</span>. Es responsabilidad de todos cuidar del entorno del bikepark y no dejar nada atrás. Hablamos de competición, pero en el mejor sentido de la palabra. Ese que hace referencia a la unión de bikers de todas las edades y diferentes puntos de la península con el fin de disfrutar de un gran fin de semana. A fondo, pero midiendo la adrenalina y con deportividad ante todo.`},

    {name: "Curso Paracaidismo",
    date: {day: 12,month: 6,year: 2021},
    icon: "./assets/icons/free-flying.svg",
    image: "https://deportesriesgo.com/wp-content/uploads/tipos-de-paracaidismo.jpeg",
    text: `Para convertirte en paracaidista tendrás que superar el curso acelerado de caída libre. Este tipo de entrenamiento es considerado en todo el mundo como el sistema más efectivo para introducirte por completo en el deporte. Nosotros te lo acercamos de la mano de <span class="special_font">SkydiveMadrid</span>. Aeródromo de OCAÑA Autovía A4, salida 62`},

    {name: "Training Day",
    date: {day: 15,month: 7,year: 2021},
    icon: "./assets/icons/jet-ski.svg",
    image: "https://i.pinimg.com/originals/45/07/8b/45078b135820c313b46600fc07623e94.jpg",
    text: `Experiencia, sin duda es lo que más nos diferencia con el resto, pero no solo la experiencia como escuela, también la experiencia de cada instructor, cada uno de ellos es profesor de kitesurf todo el año y como única profesión. ¡Garantía de satisfacción, vas a disfrutar y aprender con toda seguridad! ¡Y además solo pagas las clases o cursos que hagas, si un día no hay viento, no te encuentras bien o no te apetece, no pagas!! Tu profesor va al agua contigo, para ayudarte, aconsejarte. ¡Pocos profesores en Tarifa se mojan durante sus cursos de kitesurf, nosotros nos mojamos y estamos a tu lado! Poligono industrial La Vega 201, 11380 Tarifa, Cadiz. España`},

    {name: "Ho-Ho Day",
    date: {day: 24,month: 1,year: 2022},
    icon: "./assets/icons/snowboarding.svg",
    image: "https://img.blogs.es/ennaranja/wp-content/uploads/2016/01/deportes-invierno-6.jpg",
    text: `Acude a la estación de esquí de Navacerrada estas navidades vestido de Papá Noel, y disfrutarás gracias a <span class="special_font">SportX</span> de un descuento exclusivo para grupos a la entrada de la estación. Solicita más información rellenando el formulario que encontrarás en la sección de "Más Información" en la cabezera de esta misma web`},

    {name: "SanSilvestre",
    date: {day: 31,month: 12,year: 2021},
    icon: "./assets/icons/tachometer-alt-solid.svg",
    image: "https://upload.wikimedia.org/wikipedia/commons/f/fd/MADRID_051231_MXALX_088.jpg",
    text: "Este evento deportivo ya no necesita presentación. Despídete del año quemando todas las calorías acumuladas ese año para empezar con la cuenta a 0. Para solicitar más información, rellena el formulario al cual se accede desde la cabecera de esta misma web."
    },
];

//FINDING LIST OF SPORTS IN HTML
const infoList$$ = document.body.querySelector('.list__welcome__list');


//INCLUDING SPORTS IN LIST OF SPORTS AND ADD EVENT LISTENER
for (item of infoList) {
    const listItem$$ = document.createElement('li');
    listItem$$.className = 'list__welcome__list__item';
    const button$$ = document.createElement('a');
    button$$.className = 'tipe_button'
    const buttonIcon$$ = document.createElement('img');
    buttonIcon$$.className = 'list__welcome__list__item__icon'
    buttonIcon$$.src = item.icon;
    const buttonText$$ = document.createTextNode(item.name);
    listItem$$.appendChild(buttonIcon$$);
    button$$.appendChild(buttonText$$);
    listItem$$.appendChild(button$$);
    listItem$$.addEventListener("click", newDetailView);
    infoList$$.appendChild(listItem$$);
};


//nueva vista a mostrar 
function newDetailView (event) {
    const eventCaller = event.target.innerHTML;
    let newView = {name: eventCaller, date: '', icon: '', image: '', text: ''};

    //encuentra y almacena el item seleccionado desde el botón
    for (element of infoList) {
        if (element.name == eventCaller) {
            newView.date = element.date;
            newView.icon = element.icon;
            newView.image = element.image;
            newView.text = element.text;
        }
    }
    //llama al "dibujador" de vista
    drawNewDetailView(newView);
}

//"dibujador" de vista
function drawNewDetailView (newView) {
    const detailImage$$ = document.querySelector('.list__detail__image');
    detailImage$$.style.backgroundImage = `url('${newView.image}')`;
    const detailTitle$$ = document.querySelector('.list__detail__text__title');
    detailTitle$$.innerHTML = newView.name;

    //si no existe icono en el título (en la portada por ejemplo)...
    let detailTitleIcon$$ = document.querySelector('.list__detail__text__icon');
    if (detailTitleIcon$$ == null) {
        detailTitleIcon$$ = document.createElement('img');
        detailTitleIcon$$.className = 'list__detail__text__icon';
        detailTitle$$.innerHTML = `<img class="list__detail__text__icon" src="${newView.icon}"></img>${newView.name}`;
    } else {
        detailTitleIcon$$.innerHTML = newView.icon;
    };

    const detailText$$ = document.querySelector('.list__detail__text__content');
    detailText$$.innerHTML = newView.text;

    //si no existe fecha en el texto (en la portada por ejemplo)...
    let detailDate$$ = document.querySelector('.list__detail__text__date');
    if (detailDate$$ == null) {
        detailDate$$ = document.createElement('p');
        detailDate$$.className = 'list__detail__text__date special_text';
        detailText$$.appendChild(detailDate$$);
    };

    //si la fecha tiene día "0" es que está por determinar la fecha, si no escribe la fecha...
    newView.date.day == 0 ? detailDate$$.innerHTML = 'Próxima Fecha: Por determinar...' : detailDate$$.innerHTML = `Próxima fecha: ${newView.date.day} / ${newView.date.month} / ${newView.date.year}.`;
};
