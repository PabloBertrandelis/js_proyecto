//HELLO MESSAGE
// const slowAlert = message => { alert("Bienvenido a SportX") };
// function sayHello() { let timeoutID = setTimeout(slowAlert, 2000) };
// sayHello();

let overItems$$ = document.querySelectorAll('.home__list__item');
for (let i = 0; i < overItems$$.length; i++) {
    const item$$ = overItems$$[i];
    item$$.addEventListener('mouseover', backgroundVisible);
    item$$.addEventListener('mouseout', backgroundReturn);
    item$$.addEventListener('click', showMessage);
};

function backgroundVisible(event) {
    event.target.style.background='red';
    event.target.style.color='white';
};

function backgroundReturn(event) {
    event.target.style.background='none';
    event.target.style.color='red';
};

const image$$ = document.querySelector('.home__extraInfo__img');
const text$$ = document.querySelector('.home__extraInfo__text__info');

function showMessage (event) {
    console.log(event.target);
    if (event.target == overItems$$[0]) {
        image$$.style.backgroundImage='url(https://www.noticias-ahora.com/wp-content/uploads/2017/11/maxima-adrenalina-deportes-extremos-mundo.jpg)';
        text$$.innerHTML = 'Elige entre las actividades que te proponemos en la sección \"Actividades\". Encontrarás el link en la cabezera de la página. Busca la más adecuada para ti.';
    } else if (event.target == overItems$$[1]) {
        image$$.style.backgroundImage='url(https://www.es-paintball.com/Minis/consejos-para-jugar-paintball.jpg)';
        text$$.innerHTML = 'Puedes incluir hasta un máximo de 20 personas al evento, por lo que todo tu grupo de amigos y compañeros de fatigas pueden formar parte de esta experiencia. Apúntalos y haz de lider del grupo.';
    } else if (event.target == overItems$$[2]) {
        image$$.style.backgroundImage='url(https://stay-u-nique.com/site/wp-content/uploads/2020/04/close-up-of-a-calendar-768x538.jpeg)';
        text$$.innerHTML = 'En la descripción de la actividad tienes la fecha y una descripción de las condiciones, permisos... que son necesarios para poder disfrutar del evento. Marca esa fecha en tu calendario, y organiza tu escapada.';
    } else if (event.target == overItems$$[3]) {
        image$$.style.backgroundImage='url(https://statics-cuidateplus.marca.com/sites/default/files/concentracion-en-el-deporte.jpg)';
        text$$.innerHTML = 'La espera es la peor parte... tendrás que aguardar pacientemente hasta la fecha indicada para poder disfrutar al máximo de tu evento esperado.';
    }
};


let clearButton$$ = document.querySelector('.home__extraInfo__clearButton');
clearButton$$.addEventListener('click', clearMessage);

function clearMessage () {
    image$$.style.backgroundImage = '';
    text$$.innerHTML = `Bienvenido a la web <span class="special_font">SportX</span>, donde podrás encontrar eventos deportivos y quedadas organizadas de deportes y actividades de acción.`;
    console.log(clearButton$$);
}