
var activities = [];

for (activity of infoList) {
    activities.push(activity.name);
}

let listOfActivities$$ = document.querySelector('#activity');
let optionNull$$ = document.createElement('option');
listOfActivities$$.appendChild(optionNull$$);
for (item of activities) {
    let option$$ = document.createElement('option');
    let optionText$$ = document.createTextNode(item);
    option$$.appendChild(optionText$$);
    listOfActivities$$.appendChild(option$$);
};